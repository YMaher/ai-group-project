from collections import namedtuple
import queue
import json

# Start Copy
import sys
import base64
path = base64.b64decode(sys.argv[1])
out_file = open(path, "r")
convertedJson = json.load(out_file)
out_file.close()
# End Copy

class Node:
    def __init__(self, typeOf, heuristic, edgeIndices):
        self.typeOf = typeOf
        self.heuristic = heuristic
        self.edgeIndices = edgeIndices


class Edge:
    def __init__(self, fromNode, toNode, isDirected, weight):
        self.fromNode = fromNode
        self.toNode = toNode
        self.isDirected = isDirected
        self.weight = weight


class Path:
    def __init__(self, visited_nodes, visited_edges):
        self.visited_nodes = visited_nodes
        self.visited_edges = visited_edges


def start_processing(converted_json):
    nodes = []
    edges = []
    start_node = converted_json["startNode"]
    for node in converted_json["nodes"]:
        nodes.append(Node(node["typeOf"], node["heuristic"], node["edgeIndices"]))

    for edge in converted_json["edges"]:
        edges.append(Edge(edge["fromNode"], edge["toNode"], edge["isDirected"], edge["weight"]))
    return breadth_first_search(start_node, nodes, edges)


def breadth_first_search(start, nodes, edges):
    FringeElement = namedtuple('FringeElement', 'node_index edge_index path')
    fringe = queue.Queue()
    fringe.put(FringeElement(start, None, Path([], [])))
    visited_nodes = []
    visited_edges = []
    is_visited_node = [False for _ in range(len(nodes))]

    while not fringe.empty():
        current = fringe.get()

        if is_visited_node[current.node_index]:
            continue

        visited_edges.append(current.edge_index)
        visited_nodes.append(current.node_index)
        current.path.visited_nodes.append(current.node_index)
        current.path.visited_edges.append(current.edge_index)
        is_visited_node[current.node_index] = True

        if nodes[current.node_index].typeOf == "GOAL":
            del visited_edges[0]
            del current.path.visited_edges[0]
            #return current.path.visited_edges
            return {"visitedNodes": visited_nodes, "visitedEdges": visited_edges, "failed": False,
                    "PathvisitedNodes": current.path.visited_nodes, "PathvisitedEdges": current.path.visited_edges}

        for edge_index in nodes[current.node_index].edgeIndices:
            fringe.put(
                FringeElement(edges[edge_index].toNode, edge_index,
                              Path(current.path.visited_nodes[:], current.path.visited_edges[:])))
            if current.edge_index is not None and not edges[current.edge_index].isDirected:
                fringe.put(FringeElement(edges[edge_index].fromNode, edge_index,
                                         Path(current.path.visited_nodes[:], current.path.visited_edges[:])))

    del visited_edges[0]
    #return visited_nodes
    return {"visitedNodes": visited_nodes, "visitedEdges": visited_edges, "failed": True}


# Start Copy
out_file = open(path, "w")
json.dump(start_processing(convertedJson), out_file)
out_file.close()
# End Copy

#example
#startingNode = Node("START",1,0,[0,1,2,3])
#garboNode1 = Node ("Normal",1,0,[4])
#garboNode2 = Node ("Normal",2,0,[5])
#garboNode3 = Node ("Normal",3,0,[6])
#garboNode4 = Node ("Normal",4,0,[7])
#goalNode = Node("GOAL",0,0,[])
#nodes = [startingNode, garboNode1,garboNode2,garboNode3,garboNode4, goalNode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(0,2,True,2)
#edge2 = Edge(0,3,True,3)
#edge3 = Edge(0,4,True,4)
#edge4 = Edge(1,0,True,1)
#edge5 = Edge(2,0,True,2)
#edge6 = Edge(3,0,True,3)
#edge7 = Edge(4,5,True,10)
#edges = [edge0,edge1,edge2,edge3,edge4,edge5,edge6,edge7]

#answer = breadth_first_search(nodes,edges)

#example2
#startingnode = Node("START",1,0,[0])
#garbonode1 = Node ("Normal",1,0,[1,2])
#garbonode2 = Node ("Normal",2,0,[3])
#garbonode3 = Node ("Normal",3,0,[4])
#goalnode = Node("GOAL",0,0,[])
#nodes = [startingnode, garbonode1,garbonode2,garbonode3, goalnode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(1,4,True,8)
#edge2 = Edge(1,2,True,1)
#edge3 = Edge(2,3,True,1)
#edge4 = Edge(3,1,True,1)
#edges = [edge0,edge1,edge2,edge3,edge4]

#answer = breadth_first_search(nodes,edges)

#example3
#startingNode = Node("START",4,0,[0])
#garboNode1 = Node ("Normal",2,0,[0,1,2])
#garboNode2 = Node ("Normal",1,0,[1,3])
#goalNode = Node("GOAL",0,0,[2,3])
#nodes = [startingNode, garboNode1,garboNode2, goalNode]

#edge0 = Edge(0,1,False,1)
#edge1 = Edge(1,2,False,1)
#edge2 = Edge(1,3,False,3)
#edge3 = Edge(2,3,False,2)
#edges = [edge0,edge1,edge2,edge3]

#answer = breadth_first_search(nodes,edges)

#example4
#startingNode = Node("START",1,0,[0,1,2,3])
#garboNode1 = Node ("Normal",1,0,[4])
#garboNode2 = Node ("Normal",2,0,[5])
#garboNode3 = Node ("Normal",3,0,[6])
#garboNode4 = Node ("Normal",4,0,[7])
#goalNode = Node("GOAL",0,0,[])
#nodes = [startingNode, garboNode1,garboNode2,garboNode3,garboNode4, goalNode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(0,2,True,2)
#edge2 = Edge(0,3,True,3)
#edge3 = Edge(0,4,True,4)
#edge4 = Edge(1,5,True,1)
#edge5 = Edge(2,5,True,2)
#edge6 = Edge(3,5,True,3)
#edge7 = Edge(4,5,True,4)
#edges = [edge0,edge1,edge2,edge3,edge4,edge5,edge6,edge7]

#answer = breadth_first_search(nodes,edges)

#example5
#startingNode = Node("START",1,0,[0])
#garboNode1 = Node ("Normal",1,0,[1])
#garboNode2 = Node ("Normal",2,0,[2])
#nodes = [startingNode, garboNode1,garboNode2]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(1,2,True,1)
#edge2 = Edge(2,0,True,1)
#edges = [edge0,edge1,edge2]

#answer = breadth_first_search(nodes,edges)

#for node in answer:
#    print(node)
