import queue
import copy
import json

#jsonString = input()
#convertedJson = json.loads(jsonString)

# Start Copy
import sys
import base64
path = base64.b64decode(sys.argv[1])
out_file = open(path, "r")
convertedJson = json.load(out_file)
out_file.close()
# End Copy

def startProcessing(convertedJson):
    nodes = []
    edges = []
    for node in convertedJson["nodes"]:
        nodes.append(Node(node["typeOf"],node["heuristic"],node["depth"],node["edgeIndices"]))

    for edge in convertedJson["edges"]:
        edges.append(Edge(edge["fromNode"],edge["toNode"],edge["isDirected"],edge["weight"]))
    return bestFit(nodes,edges)

class Node:
    def __init__(self,typeOf,heuristic,depth,edgeIndices):
        self.typeOf=typeOf
        self.heuristic= heuristic
        self.depth = depth
        self.edgeIndices=edgeIndices

    def __str__(self):
        return (self.typeOf)

class Edge:
    def __init__(self,fromNode,toNode,isDirected,weight):
        self.fromNode=fromNode
        self.toNode=toNode
        self.isDirected=isDirected
        self.weight=weight

class Path:
    def __init__(self, Cost):
        self.visitedNodes= []
        self.returnedVisitedNodes= []
        self.edgesTaken = []
        self.Cost = Cost
    def __lt__(self, other):
       selfPriority = self.Cost
       otherPriority = other.Cost
       return selfPriority < otherPriority


def bestFit(nodes,edges):
    global convertedJson
    startingNode = nodes[convertedJson["startNode"]]
    #startingNode = None
    goalNodes = []
    fringe = queue.PriorityQueue()
    visitedNodes = []
    returnedVisitedNodes = []
    edgesTaken = []
    
    for node in range(len(nodes)):
        #if (nodes[node].typeOf == "START"):
        #    startingNode = nodes[node]

        if(nodes[node].typeOf == "GOAL"):
            goalNodes.append(nodes[node])

    start = Path(startingNode.heuristic)
    fringe.put((start,startingNode,None))
    
    while(not fringe.empty()):
        Priority = fringe.get()
        currentNode = Priority[1]
        print(nodes.index(currentNode), Priority[2] ,Priority[0].Cost)

        if (currentNode in Priority[0].visitedNodes):
            continue
  
        Priority[0].visitedNodes.append(currentNode)
        Priority[0].edgesTaken.append(Priority[2])
        Priority[0].returnedVisitedNodes.append(nodes.index(currentNode))
        visitedNodes.append(currentNode)
        returnedVisitedNodes.append(nodes.index(currentNode))
        edgesTaken.append(Priority[2])
        
        
        if (currentNode in goalNodes):
            del edgesTaken[0]
            del Priority[0].edgesTaken[0]
            #return Priority[0].returnedVisitedNodes
            return {"visitedNodes" : returnedVisitedNodes, "visitedEdges" : edgesTaken, "failed": False ,"PathvisitedNodes" : Priority[0].returnedVisitedNodes ,"PathvisitedEdges" : Priority[0].edgesTaken}

        for edgeIndex in currentNode.edgeIndices:
            if (edges[edgeIndex].isDirected):
                targetNode = edges[edgeIndex].toNode
            else:
                if (nodes[edges[edgeIndex].toNode] == currentNode):  
                    targetNode = edges[edgeIndex].fromNode
                else:
                    targetNode = edges[edgeIndex].toNode

            TempPath = Path(nodes[targetNode].heuristic)
            TempPath.visitedNodes = [i for i in Priority[0].visitedNodes]
            TempPath.returnedVisitedNodes = [i for i in Priority[0].returnedVisitedNodes]
            TempPath.edgesTaken = [i for i in Priority[0].edgesTaken]
            fringe.put((TempPath,nodes[targetNode],edgeIndex))
            
    del  edgesTaken[0]
    #return returnedVisitedNodes
    return {"visitedNodes": returnedVisitedNodes, "visitedEdges": edgesTaken, "failed": True}


# Start Copy
out_file = open(path, "w")
json.dump(startProcessing(convertedJson),out_file)
out_file.close()
# End Copy

#jsonconversion
#answer =json.dumps(startProcessing(convertedJson))
#print (answer)

#example
#startingNode = Node("START",1,0,[0,1,2,3])
#garboNode1 = Node ("Normal",1,0,[4])
#garboNode2 = Node ("Normal",2,0,[5])
#garboNode3 = Node ("Normal",3,0,[6])
#garboNode4 = Node ("Normal",4,0,[7])
#goalNode = Node("GOAL",0,0,[])
#nodes = [startingNode, garboNode1,garboNode2,garboNode3,garboNode4, goalNode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(0,2,True,2)
#edge2 = Edge(0,3,True,3)
#edge3 = Edge(0,4,True,4)
#edge4 = Edge(1,0,True,1)
#edge5 = Edge(2,0,True,2)
#edge6 = Edge(3,0,True,3)
#edge7 = Edge(4,5,True,10)
#edges = [edge0,edge1,edge2,edge3,edge4,edge5,edge6,edge7]

#answer = bestFit(nodes,edges)

#example2
#startingnode = Node("START",1,0,[0])
#garbonode1 = Node ("Normal",1,0,[1,2])
#garbonode2 = Node ("Normal",2,0,[3])
#garbonode3 = Node ("Normal",3,0,[4])
#goalnode = Node("GOAL",0,0,[])
#nodes = [startingnode, garbonode1,garbonode2,garbonode3, goalnode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(1,4,True,8)
#edge2 = Edge(1,2,True,1)
#edge3 = Edge(2,3,True,1)
#edge4 = Edge(3,1,True,1)
#edges = [edge0,edge1,edge2,edge3,edge4]

#answer = bestFit(nodes,edges)

#example3
#startingNode = Node("START",4,0,[0])
#garboNode1 = Node ("Normal",2,0,[0,1,2])
#garboNode2 = Node ("Normal",1,0,[1,3])
#goalNode = Node("GOAL",0,0,[2,3])
#nodes = [startingNode, garboNode1,garboNode2, goalNode]

#edge0 = Edge(0,1,False,1)
#edge1 = Edge(1,2,False,1)
#edge2 = Edge(1,3,False,3)
#edge3 = Edge(2,3,False,2)
#edges = [edge0,edge1,edge2,edge3]

#answer = bestFit(nodes,edges)

#example4
#startingNode = Node("START",1,0,[0,1,2,3])
#garboNode1 = Node ("Normal",1,0,[4])
#garboNode2 = Node ("Normal",2,0,[5])
#garboNode3 = Node ("Normal",3,0,[6])
#garboNode4 = Node ("Normal",4,0,[7])
#goalNode = Node("GOAL",0,0,[])
#nodes = [startingNode, garboNode1,garboNode2,garboNode3,garboNode4, goalNode]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(0,2,True,2)
#edge2 = Edge(0,3,True,3)
#edge3 = Edge(0,4,True,4)
#edge4 = Edge(1,5,True,1)
#edge5 = Edge(2,5,True,2)
#edge6 = Edge(3,5,True,3)
#edge7 = Edge(4,5,True,4)
#edges = [edge0,edge1,edge2,edge3,edge4,edge5,edge6,edge7]

#answer = bestFit(nodes,edges)

#example5
#startingNode = Node("START",1,0,[0])
#garboNode1 = Node ("Normal",1,0,[1])
#garboNode2 = Node ("Normal",2,0,[2])
#nodes = [startingNode, garboNode1,garboNode2]

#edge0 = Edge(0,1,True,1)
#edge1 = Edge(1,2,True,1)
#edge2 = Edge(2,0,True,1)
#edges = [edge0,edge1,edge2]

#answer = bestFit(nodes,edges)

#for node in answer:
#    print(node)