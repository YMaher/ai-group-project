using System;
using UnityEngine;
using UnityEngine.UI;

// [RequireComponent(typeof(RectTransform))]
[Serializable]
public class Edge : MonoBehaviour
{
    enum EdgeState {
        NORMAL,
        REVERSED,
        TWO_WAY
    }

    public bool assigned {
        get { return fromNode != null && toNode != null; }
    }

    public bool isDirected {
        get { return state != EdgeState.TWO_WAY; }
    }

    public float weight {
        get { return float.Parse(weightInputField.text); }
    }

    public bool interactable {
        get {
            return editable;
        }
        set {
            editable = value;
            weightInputField.interactable = value;
        }
    }

    public Color? color {
        get {
            return coloredImage.color;
        }
        set {
            coloredImage.color = value == null ? defaultColor : value.Value;
        }
    }
    
    public Node fromNode { get; private set; } = null;
    public Node toNode { get; private set; } = null;

    [SerializeField] private Image coloredImage;
    [SerializeField] private RectTransform lineRectTransform = null;
    [SerializeField] private RectTransform extraArrowhead = null;
    [SerializeField] private InputField weightInputField = null;
    [SerializeField] private float assignedLengthOffset = 0;

    private bool editable = true;
    private string weightDefaultValue;
    private EdgeState state = EdgeState.NORMAL;
    private Color defaultColor;

    public void AssignNode(Node node)
    {
        if (fromNode == null)
            fromNode = node;
        else if (toNode == null)
        {
            if(node != fromNode)
            {
                toNode = node;
                UIManager.singleton.SetEditingAllowed(true);
            }
            else print("Cannot set node as both fromNode and toNode of edge");
        }
        else
            Debug.LogError("Cannot set edge node (edge already assigned)");
    }

    public void PointerClick()
    {
        if(!editable) return;
        
        if(UIManager.singleton.deleteFlag)
        {
            UIManager.singleton.DeleteEdge(this);
            return;
        }

        state = (EdgeState)((int)(state+1) % Enum.GetNames(typeof(EdgeState)).Length);

        extraArrowhead.gameObject.SetActive(state == EdgeState.TWO_WAY);
        if(state != EdgeState.TWO_WAY)
            (fromNode, toNode) = (toNode, fromNode);
    }
    
    public void WeightUpdated()
    {
        if(weightInputField.text.Length == 0)
            weightInputField.text = weightDefaultValue;            
    }

    private void Start()
    {
        if(lineRectTransform == null)
            Debug.LogWarning("lineRectTransform not assigned and will never be assigned");
        weightDefaultValue = weightInputField.text;
        defaultColor = color.Value;
    }

    private void Update()
    {
        if(fromNode != null)
        {
            Vector2 sourcePos = fromNode.transform.position;
            Vector2 targetPos = !assigned ? Input.mousePosition : toNode.transform.position;
            
            lineRectTransform.sizeDelta = new Vector2(Vector2.Distance(sourcePos, targetPos) / ScrollviewZoom.singleton.currentScale, lineRectTransform.sizeDelta.y);
            if(assigned)
                lineRectTransform.sizeDelta += Vector2.right * assignedLengthOffset;

            transform.position = Vector2.Lerp(sourcePos, targetPos, 0.5f);
            transform.rotation = Quaternion.LookRotation((targetPos - sourcePos).normalized);

            weightInputField.transform.rotation = Quaternion.identity;

            if(!assigned && Input.GetKey(KeyCode.Escape))
            {
                UIManager.singleton.DeleteEdge(this);
                UIManager.singleton.SetEditingAllowed(true);
            }
        }
    }
}
